# Epic Music Manager

## Exemples d'utilisation :

https://www.youtube.com/playlist?list=PL4GuozAy0yqdd7SGYhDOaTF7GNYhxHFmQ

De l'aide pour les modules est disponible via un click droit en sélectionnant l'option "Aide".

## Modules pédale d'effet numérique

### Prérequis

* Brancher son instrument.
* Avoir installé PureData
    ```bash
       sudo apt install puredata
    ```
### Utiliser PureData
* Lancer un nouveau terminal et lancer puredata en mode temps réel :
    ```bash
       sudo puredata -rt
    ```
* Dans puredata selectionner les entrées sorties :

Cliquer sur le menu media et selectionner "Paramètres audio..."

![Media](/ressources/images/media.png)

À ce stade, selectionner les interfaces d'entrée et de sortie

![Peripherique](/ressources/images/peripherique.png)

* Ouvrir l'objet "manager.pd" via l'option ouvrir de puredata
* Les explication sur les différents modules sont disponibles en faisant un clic droit sur l'objet contenant les modules et en selectionnant "aide".

### Utilisation avec JACK

* Il faut nécéssairement avoir installé le serveur jackd
    ```bash
        sudo apt install jackd
    ```
* Chercher l'interface audio
    ```bash
        aplay -l
                **** Liste des Périphériques Matériels PLAYBACK ****
        carte 0: PCH [HDA Intel PCH], périphérique 0: ALC257 Analog [ALC257 Analog]
          Sous-périphériques: 0/1
          Sous-périphérique #0: subdevice #0
        carte 0: PCH [HDA Intel PCH], périphérique 3: HDMI 0 [HDMI 0]
          Sous-périphériques: 1/1
          Sous-périphérique #0: subdevice #0
        carte 0: PCH [HDA Intel PCH], périphérique 7: HDMI 1 [HDMI 1]
          Sous-périphériques: 1/1
          Sous-périphérique #0: subdevice #0
        carte 0: PCH [HDA Intel PCH], périphérique 8: HDMI 2 [HDMI 2]
          Sous-périphériques: 1/1
          Sous-périphérique #0: subdevice #0
        carte 0: PCH [HDA Intel PCH], périphérique 9: HDMI 3 [HDMI 3]
          Sous-périphériques: 1/1
          Sous-périphérique #0: subdevice #0
        carte 0: PCH [HDA Intel PCH], périphérique 10: HDMI 4 [HDMI 4]
          Sous-périphériques: 1/1
          Sous-périphérique #0: subdevice #0
        carte 1: USB [Scarlett Solo USB], périphérique 0: USB Audio [USB Audio]
          Sous-périphériques: 1/1
          Sous-périphérique #0: subdevice #0
    ```

#### Utilisation en ligne de commandes
    
* Lancer le serveur jackd
    ```bash
        sudo jackd -Rd alsa -d hw:<numéro de carte> -r 48000 -p 1024 -n 2
    ```
* Lancer puredata en temps réel avec jack comme serveur son
    ```bash
        sudo puredata -rt -jack -r 48000 -alsamidi
    ```

#### Utilisation par interface graphique

* Lancer qjackctl
    ```bash
        qjackctl
    ```
    
    ![qjackctl](/ressources/images/qjackctl.png)
    
* Connecter les entrées / sorties convenablement

    ![qjackctlconn](/ressources/images/qjackctlconn.png)

* Lancer puredata
    ```bash
        puredata
    ```
    

### Bugs et travail restant

- V gestion de la capture de signal en vue de son enregistrement
- effets :
	+ overdrive
	+ fadein/fadeout (temps de changement de volume)
	+ 1/2 looper
- ajout :
    + equalizer personalisé (production de filtres se modifiant au fil du temps / pourrait faire le fadein/out avec effet wawa)
